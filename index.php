<?php
	include_once 'modules/session.php';

	$session->doCheckSession();

	if($session->isLoggedIn) {
		die($session->public_data['username']);
	} else
		die($session->err_message);
?>
<!doctype html>
<html>
	<head>
		<title>Main page</title>
	</head>
	<body>
		<a href="signup/">Sign up</a>
	</body>
</html>