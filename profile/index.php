<?php
	include_once '../modules/session.php';
	include_once '../modules/profiles.php';

	$session->doCheckSession();

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
		$profile->setData(trim($id));
		$profile->doGetData();
	} else if(isset($_SERVER['REQUEST_URI'])) {
		$request = explode('/',$_SERVER['REQUEST_URI']);
		if(isset($request[3]) && trim($request[3]) == "") {
			if($session->isLoggedIn) header('Location: '.$session->public_data['username']);
			else header('Location: /comic/');
		}
		$profile->setData($request[3]);
		$profile->doGetData();
	} else {
		// nothing?
	}
	
	if(!$profile->found) 
		die($profile->errmsg);
?>
<!doctype html>
<html>
	<head>
		<title><?php echo $profile->profile['username']; ?>'s profile</title>
	</head>
	<body>
		<?php echo $profile->profile['fname']; ?>
	</body>
</html>