<?php
	include_once '../modules/session.php';
	include_once '../modules/discussions.php';

	$session->doCheckSession();
?>
<!doctype html>
<html>
<head>
	<title>Discussions Area</title>
</head>
<body>
	<div>
		<ul id="discussions">
			<?php 	if(!isset($_GET['threadid'])) 
						include_once '../includes/get-discussion.php';
					else
						include_once '../includes/get-thread.php';
			?>
		</ul>
	</div>
	<?php if(!isset($_GET['threadid']) && $session->isLoggedIn) include_once '../includes/post-discussion.php'; ?>
	<script src="../assets/js/jquery.min.js"></script>
	<?php if(!isset($_GET['threadid']) && $session->isLoggedIn) include_once '../includes/post-discussion-scripts.php'; ?>
</body>
</html>