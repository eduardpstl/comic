<?php
	/* the main method with which we see what the user tried to do */
	if(!isset($_GET['action_id']))
		die('err1'); // no action specified
	if(trim($_GET['action_id']) == '')
		die('err2'); // the action id cannot be empty
	$action_id = trim($_GET['action_id']); // we get the action id, and check it while we're at it
	if(!is_numeric($action_id))
		die('err3'); // the action id is supposed to be numeric
	include_once '../assets/php/session.php';
	include_once '../assets/php/discussions.php';
	/* check if the user is logged in */
	$session->doCheckSession();
	/* there are certain actions which only logged in users can do */
	switch($action_id) {
		case 100: $discussions->postDiscussion((isset($_POST['title'])) ? trim($_POST['title']) : '',
												 (isset($_POST['category'])) ? trim($_POST['category']) : 0,
												 (isset($_POST['tags'])) ? trim($_POST['tags']) : '',
												 (isset($_POST['post'])) ? trim($_POST['post']) : '',
												 (isset($_POST['lang'])) ? trim($_POST['lang']) : 0,
												 $session); break;
		case 101: $discussions->postReply(isset($_POST['post']) ? trim($_POST['post']) : '', isset($_POST['quoting']) ? trim($_POST['quoting']) : null, isset($_POST['threadid']) ? trim($_POST['threadid']) : 0, $session); break;
		case 200: echo $discussions->getAllDiscussions(isset($_GET['page']) ? trim($_GET['page']) : 0, $session); break;
		case 202: echo $discussions->getNumberOfThreads((isset($_GET['categories']) && trim($_GET['categories']) != "") ? json_decode(trim($_GET['categories']), true) : array(), (isset($_GET['tags']) && trim($_GET['tags']) != "") ? json_decode(trim($_GET['tags']), true) : array()); break;
		case 300: $discussions->deleteThread(isset($_POST['threadid']) ? trim($_POST['threadid']) : 0, $session); break;
		case 301: $discussions->deletePost(isset($_POST['postid']) ? trim($_POST['postid']) : 0, $session); break;
		case 400: $discussions->editThread($_POST['threadid'], array('post' => trim($_POST['post'])), $session); break;
		case 401: $discussions->editPost($_POST['postid'], array('post' => trim($_POST['post'])), $session); break;
		default: die('err5'); // wrong action id
	}
?>