<?php
	$thread = $discussions->getThread(1, $_GET['threadid'], $session);

	if(!$thread) {
		echo 'No posts to load.';
	} else {
		echo $thread['thread']['title'].'</br>';
		echo $thread['thread']['post'];
		$posts = $thread['posts']; echo '<ul>';
		foreach($posts as $post) {

			$toPost = '<li><div class="post">';
			$toPost .= '<input type="hidden" data-threadid="'.$post['postid'].'">';
			$toPost .= '<div>'.$post['post'].'</div>';
			$toPost .= '</div></li>';

			echo $toPost;
		}

		echo '</ul>';
	}
?>