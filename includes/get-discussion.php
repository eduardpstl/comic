<?php
	$alldisc = $discussions->getAllDiscussions(1, $session);

	if(!$alldisc) {
		echo 'No posts to load.';
	} else {
		foreach($alldisc as $discussion) {
			$toPost = '<li style="background: '.$discussion['cat_color'].'"><div class="discussion">';
			$toPost .= '<input type="hidden" data-threadid="'.$discussion['did'].'">';
			$toPost .= '<a href="?threadid='.$discussion['did'].'">'.$discussion['title'].'</a>';
			$toPost .= '</div></li>';

			echo $toPost;
		}
	}
?>