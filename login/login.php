<?php
	require_once '../modules/login.php';
	include_once '../modules/dbcon.php';

	$li = new LogIn();
	$li->setDbCon($dbcon);
	$li->shouldKeepLoggedIn(true);
	$li->setData($_POST['login_name'], $_POST['pass']);
	$li->doLogIn();
?>