<!doctype html>
<html>
	<head>
		<title>Log in page</title>
	</head>
	<body>
		<input type="text" placeholder="username or email" id="login_name">
		<input type="password" placeholder="password" id="pass">
		<button id="submit">Submit</button>
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/sha512.js"></script>
		<script>
			$('#submit').on('click', function() {
				var login = $('#login_name').val();
				var pass = $('#pass').val();

				$.post('login.php','login_name='+login+'&pass='+CryptoJS.SHA512(login + pass + window.location.origin));
			});
		</script>
	</body>
</html>