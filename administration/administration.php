<?php
	class Administration {
		private $dbcon;
		public $errmsg;

		public function __construct() {
			$this->dbcon = null;
		}

		public function getLatestUsers($howmany = 0, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* check to see if we have an actual number, even though we should have... */
			$number = $this->isNumber($howmany, 'err1'); // not a number
			/* check session */
			if(!$session || !isset($session->public_data))
				return $this->error('err2'); // not logged in
			/* check rank... */
			if($session->public_data['rank'] < 2)
				return $this->error('err3'); // not enough rights to do this
			if($number) {
				/* prepare the query */
				$query = "SELECT username,email,signup_date FROM accounts ORDER BY signup_date DESC LIMIT 0,$number";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result || !mysqli_num_rows($result))
					return $this->error('err4'); // couldn't find an user or could not complete the query...
				/* put everything in a nice to use json */
				$array = array();
				for($i = 0; $i < mysqli_num_rows($result); $i++)
					$array[$i] = mysqli_fetch_assoc($result);
				return json_encode($array);
			} else return null;
		}

		public function promoteOrDemoteUser($userid = 0, $newrank = 0, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* the numbers... */
			$userid = $this->isNumber($userid, 'err1'); // invalid id
			$newrank = $this->isNumber($newrank, 'err1');
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err2'); // not logged in
			/* the rank */
			if($session->public_data['rank'] < 4)
				return $this->error('err3'); // don't have the rights for this
			/* check if the user exits... */
			$query = "SELECT rank FROM accounts WHERE aid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err4');
		}

		public function banUser($userid = 0, $userip = '', $bantype = 0, $groupid = null, $description = '', $duration = 0, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* the numbers... */
			$userid = $this->isNumber($userid, 'err1'); // invalid id
			$bantype = $this->isNumber($bantype, 'err1');
			if($groupdid) $groupdid = $this->isNumber($groupdid, 'err1');
			$duration = $this->isNumber($duration, 'err1');
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err2'); // not logged in
			/* the rank */
			if($session->public_data['rank'] < 2)
				return $this->error('err3'); // don't have the rights for this
			/* check if the user exits... */
			$query = "SELECT rank FROM accounts WHERE aid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err4');
			if($session->public_data['rank'] <= mysqli_fetch_assoc($result)['rank'])
				return $this->error('err5'); // you cannot ban someone equal or higher rank than you
			if($userid && $bantype && $duration && $description != '') {
				/* escape the description */
				$description = mysqli_real_escape_string($this->dbcon, htmlentities($description));
				/* and the insert query */
				$modid = $session->public_data['id'];
				$query = "";
				if($groupid)
					$query = "INSERT INTO bans VALUES (null,$userid,$bantype,'$description',$groupid,'$userip',$modid)";
				else 
					$query = "INSERT INTO bans VALUES (null,$userid,$bantype,'$description',null,'$userip',$modid)";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err6'); // couldn't ban...
				return 'ok';
			} else return $this->error('err7'); // invalid parameters
		}

		private function isNumber($number, $msg) {
			if(!preg_match('/[0-9]+/', $number))
				$this->error($msg);
			$number = intval($number);
			if(!is_numeric($number))
				$this->error($msg);
			if(!$number)
				$this->error($msg);
			return $number;
		}

		private function error($msg) {
			$this->errmsg = $msg;
			return null;
		}		
		private function noDb() {
			die('err001');
		}	
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}

	$admin = new Administration();
	@include_once '../modules/dbcon.php';
	$admin->setDbCon($dbcon);
?>