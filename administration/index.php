<?php 
	include_once '../modules/session.php';

	$session->doCheckSession();

	if(!$session->isLoggedIn)
		die('...'); // not logged in, lol
	if($session->public_data['rank'] < 2)
		die('...'); // you don't have the rank, lol

	include_once 'administration.php';

	$users = $admin->getLatestUsers(10, $session);
	if($users == null)
		die($admin->errmsg);

	die($users);
?>