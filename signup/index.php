<!doctype html>
<html>
	<head>
		<title>Sign up page</title>
	</head>
	<body>
		<div id="container">
			<div id="page1">
				<label for="fname">First name</label>
				<input type="text" id="fname" placeholder="ex: John">
				<label for="lname">Last name</label>
				<input type="text" id="lname" placeholder="ex: Smith">
			</div>
			<div id="page2">
				<label for="username">Your username</label>
				<input type="text" id="username" placeholder="ex: John21">
				<label for="email">Your email</label>
				<input type="email" id="email" placeholder="john.smith@example.com">
				<label for="reemail">Repeat your email</label>
				<input type="email" id="reemail">
			</div>
			<div id="page3">
				<label for="pass">Password</label>
				<input type="password" id="pass" placeholder="Use multiple types of characters">
				<label for="repass">Repeat the password</label>
				<input type="password" id="repass">	
			</div>
			<div id="page4">
				<!-- recaptcha -->
				<?php
					/* code from the recaptcha php documentation - https://developers.google.com/recaptcha/docs/php */
				  require_once('../assets/php/recaptcha/recaptchalib.php');
				  include_once('../assets/php/recaptcha/recaptchakeys.php');
				  echo recaptcha_get_html($publicre_key);
				 ?>
			</div>
			<div id="page5">
				<button id="submit">Submit</button>
			</div>
		</div>
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/sha512.js"></script>
		<script>
			$('#submit').on('click', function() {
				var fname = $('#fname').val();
				var lname = $('#lname').val();
				var username = $('#username').val();
				var email = $('#email').val();
				var reemail = $('#reemail').val();
				var pass = $('#pass').val();
				var repass = $('#repass').val();
				var cid = $('#recaptcha_challenge_field').val();
				var cresp = $('#recaptcha_response_field').val();

				var hash = CryptoJS.SHA512(email + pass + window.location.origin);

				$.post('signup.php','fname='+fname+'&lname='+lname+'&username='+username+'&email='+email+'&pass='+hash+'&cid='+cid+'&cresp='+cresp);
			
				Recaptcha.reload();
			});
		</script>
	</body>
</html>