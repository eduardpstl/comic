<?php
	if(!isset($_POST['email']))
		die('err1'); // missing an argument
	if(!isset($_POST['pass']))
		die('err1');
	if(!isset($_POST['username']))
		die('err1');
	if(!isset($_POST['cid']) || !isset($_POST['cresp']))
		die('err2'); // captcha response missing or wrong
	if(!isset($_POST['fname']) || !isset($_POST['lname']))
		die('err1');

	require_once '../modules/signup.php';
	require_once '../modules/dbcon.php';

	$su = new SignUp();
	$su->setDbCon($dbcon);
	$su->setCaptcha($_POST['cid'],$_POST['cresp']);
	$su->setData($_POST['fname'],$_POST['lname'],$_POST['username'],$_POST['email'],$_POST['pass']);
	$su->doSignUp();

	die('allokay');
?>