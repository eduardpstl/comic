<?php
	function updateJson($path, $arr) {
		$file = @file_get_contents($path);
		if(!$file)
			die('err1');
		$us = unserialize($file);
		$json = json_decode($us, true);
		$json[count($json)] = $arr;
		$json = json_encode($json);
		$s = serialize($json);
		$file = @file_put_contents($path, $s);
		if(!$file)
			die('err2');
	}
?>