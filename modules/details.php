<?php
	class Details {
		private $dbcon;

		public function __construct() {
			$this->dbcon = null;
		}

		public function getDetails($userid = 0) {
			if(!$this->dbcon)
				$this->noDb();
			/* the user id */
			$userid = $this->isNumber($userid);
			if($userid == null)
				return $this->error('err1'); // not a number
			/* the query */
			$query = "SELECT * FROM details WHERE userid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err3'); // could not get the results
			return json_encode(mysqli_fetch_assoc($result));
		}

		public function editDetails($toedit = array(), $privacy = 0, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err2'); // user not logged in?
			/* check the privacy... */
			$privacy = $this->isNumber($privacy);
			if($privacy == null)
				return $this->error('err1');
			/* check the number of elements to change */
			if(!count($toedit))
				return $this->error('err5'); // nothing to change?
			/* check to see if the user actually exists */
			$userid = $session->public_data['id'];
			$query = "SELECT * FROM details WHERE userid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err4'); // user not found
			$user = mysqli_fetch_assoc($result);
			/* the patterns */
			$name_patt = '/[a-zA-Z .-]+/';
			$alnu_patt = '/[a-zA-Z0-9 .-]+/';
			/* and finally, we build the update query */
			$query = "UPDATE details SET ";
			if(isset($toedit['fname']))
				if(preg_match($name_patt, $toedit['fname']))
					$query .= "fname='".mysqli_real_escape_string($this->dbcon, $toedit['fname'])."', ";
				else return $this->error('err100'); // that doesn't look like a name
			if(isset($toedit['lname']))
				if(preg_match($name_patt, $toedit['lname']))
					$query .= "lname='".mysqli_real_escape_string($this->dbcon, $toedit['lname'])."', ";
				else return $this->error('err101'); // that doesn't look like a name
			if(isset($toedit['city']))
				if(preg_match($name_patt, $toedit['city']))
					$query .= "city='".mysqli_real_escape_string($this->dbcon, $toedit['city'])."', ";
				else return $this->error('err102'); // that doesn't look like a name
			if(isset($toedit['country']))
				if(preg_match($name_patt, $toedit['country']))
					$query .= "country='".mysqli_real_escape_string($this->dbcon, $toedit['country'])."', ";
				else return $this->error('err103'); // that doesn't look like a name
			if(isset($toedit['profession']))
				if(preg_match($name_patt, $toedit['profession']))
					$query .= "profession='".mysqli_real_escape_string($this->dbcon, $toedit['profession'])."', ";
				else return $this->error('err104'); // that doesn't look like a name
			if(isset($toedit['company']))
				if(preg_match($name_patt, $toedit['company']))
					$query .= "company='".mysqli_real_escape_string($this->dbcon, $toedit['company'])."', ";
				else return $this->error('err105'); // that doesn't look like a name
			if(isset($toedit['hobbies']))
				$query .= "hobbies='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['hobbies']))."', ";
			if(isset($toedit['interests']))
				$query .= "interests='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['interests']))."', ";
			if(isset($toedit['bio']))
				$query .= "bio='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['bio']))."', ";
			if(isset($toedit['birthday']))
				if($this->isNumber($toedit['birthday']) != null && ($toedit['birthday'] < 1 || $toedit['birthday'] > 31))
					$query .= "birthday=".$toedit['birthday'].", ";
				else return $this->error('err106'); // doesn't look like a day
			if(isset($toedit['birthmonth']))
				if($this->isNumber($toedit['birthmonth']) != null && ($toedit['birthmonth'] < 1 || $toedit['birthmonth'] > 12))
					$query .= "birthmonth=".$toedit['birthmonth'].", ";
				else return $this->error('err106'); // doesn't look like a month
			if(isset($toedit['birthyear']))
				if($this->isNumber($toedit['birthyear']) != null)
					$query .= "birthyear=".$toedit['birthyear'].", ";
				else return $this->error('err106'); // doesn't look like an year
			/* add the privacy and the time the details were edited */
			$query .= "privacy=$privacy,edited=$time WHERE userid=$userid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return $this->error('err6'); // could not update the details.. bad query?
		}

		private function isNumber($number) {
			if(!preg_match('/[0-9]+/', $number))
				return null;
			$number = intval($number);
			if(!is_numeric($number))
				return null;
			if(!$number)
				return null;
			return $number;
		}

		private function noDb() {
			die('err001');
		}
		private function error($msg) {
			$this->errmsg = $msg;
			return null;
		}
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}

	$details = new Details();
	@include_once 'dbcon.php';
	$details->setDbCon($dbcon);
?>