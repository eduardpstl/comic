<?php
	class SignUp {
		private $data = array();
		private $dbdata;
		private $captcha = array();
		private $dbcon;

		/* the constructor - initializing everything */
		public function __construct() {
			$this->data = array('username' => "",
								'email' => "",
								'pass' => "",
								'org_pass' => "",
								'salt' => "",
								'signup_date' => "",
								'last_login' => "");
			$this->captcha = array('cid' => "", 'cresp' => "");
			$this->dbcon = null;
		}

		/* the main method, which actually starts step by step the sign up process/methods */
		public function doSignUp() {
			if(!$this->dbcon) die('err001'); // could not connect to the db
			$this->escapeData();
			$this->checkCaptcha();
			$this->registerPrivateData();
			$this->registerPassword();
			$this->updateDB();
			$this->logInUser();
		}
		public function checkAvailability() {
			if(!$this->dbcon) die('err001');
			$this->escapeUsernameAndEmail();
			$this->checkUsernameAndEmail();
		}
		/* method which will do all the small checks and escape the received data */
		private function escapeData() {
			/* first, the small checks */
			if($this->data['pass'] == '')
				die('err7'); // empty password
			/* little method used in checking the username's and email's availability too */
			$this->escapeUsernameAndEmail();
			/* and now that we've done the small things, we'll continue with the more indepth checks and start the proper registration */
		}
		/* this method will check the captcha - it has the highest possibility of being missed 
		/ entered incorectly, so we should stop the script from running before doing the other checks */
		private function checkCaptcha() {
			/* code from the recaptcha php documentation - https://developers.google.com/recaptcha/docs/php */
			require_once('recaptcha/recaptchalib.php');
			require_once('recaptcha/recaptchakeys.php');
			$resp = recaptcha_check_answer ($privatere_key,
			                          $_SERVER["REMOTE_ADDR"],
			                          $this->captcha['cid'],
			                          $this->captcha['cresp']);

			if (!$resp->is_valid)
				die('err2'); // invalid captcha response
			/* if the script didn't die until now, we'll continue */
		}
		/* the method which will check the username, email (if they exist in the db already and their form) 
		and fname and lname */
		private function registerPrivateData() {
			/* we use this method in the check of an username's or email's availability too */
			$this->checkUsernameAndEmail();
		}
		/* the method which generates a salt, and makes the final hash */
		private function registerPassword() {
			$this->data['org_pass'] = $this->data['pass'];
			/* let's generate a salt */
			$this->data['salt'] = uniqid(mt_rand(), true);
			/* now, let's process the hash we receive from the client a bit */
			$half = (int)(strlen($this->data['pass']) / 2);
			$first_h = substr($this->data['pass'], 0, $half);
			$second_h = substr($this->data['pass'], $half); // we break it into two 
			$hash = hash('sha512', $second_h.$this->data['salt'].$this->data['username'].$first_h); // and then reverse the halves and put the salt and username in between them
			$this->data['pass'] = $hash;
			/* nothing to die for */
		}
		/* and finally, the method which will insert the new user in the database */
		private function updateDB() {
			/* the variables */
			$un = $this->data['username'];
			$em = $this->data['email'];
			$pw = $this->data['pass'];
			$st = $this->data['salt'];
			$sd = time();
			/* let's create the query */
			$query = "INSERT INTO accounts (username,email,password,salt,signup_date,last_login,rank) 
						VALUES ('$un','$em','$pw','$st',$sd,$sd,1,1)";
			/* execute it */
			$result = mysqli_query($this->dbcon, $query);
			/* and check to see if it went out without any errors */
			if(!$result)
				die('err10'); // the creation could not be completed, please try again
			/* we create the group cache the user has */
			$someid = mysqli_insert_id($this->dbcon);
			if(!is_numeric($someid) && $someid == 0) return $this->error('err11'); // some error...
			$json = json_encode(array());
			$s = serialize($json);
			$file = @file_put_contents('../assets/caches/users/'.sha1($someid), $s);
			/* else, we can continue with logging in the user for the first time */
		}
		/* and the last method of the process will log in the user if the sign up is successful */
		private function logInUser() {
			include_once 'login.php';

			$li = new LogIn();
			$li->setDbCon($this->dbcon);
			$li->setData($this->data['email'], $this->data['org_pass']);
			$li->shouldKeepLoggedIn(true);
			$li->doLogIn();
		}

		private function escapeUsernameAndEmail() {
			if($this->data['username'] == '')
				die('err4'); // username invalid or empty
			if($this->data['email'] == '')
				die('err6'); // email empty or invalid
			/* escape the username */
			$this->data['username'] = mysqli_real_escape_string($this->dbcon, $this->data['username']);
		}

		private function checkUsernameAndEmail() {
			/* check the username */
			$pattern = '/[a-zA-Z][a-zA-Z0-9\\-_]{2,20}/'; // the usernames should be at least 3 letters and at most 20, can contain numbers and hyphens
			if(preg_match($pattern, $this->data['username']) == 0 || FALSE)
				die('err4');
			/* and now, we check the email */
			$pattern = '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/'; // standard email regex, found on stackoverflow - http://stackoverflow.com/questions/201323/using-a-regular-expression-to-validate-an-email-address/1917982#1917982
			if(preg_match($pattern, $this->data['email']) == 0 || FALSE)
				die('err6');
			/* now that we finished the basic checking, we can start checking the availability in the db */
			$un = $this->data['username'];
			$em = $this->data['email'];
			/* first we check the username's availability */
			$query = "SELECT username FROM accounts WHERE LOWER(username)=LOWER('$un') LIMIT 1"; // the query
			$result = mysqli_query($this->dbcon, $query); // the db query
			if(mysqli_num_rows($result) > 0)
				die('err8'); // username taken
			/* and now we do the same thing with the email */
			$query = "SELECT email FROM accounts WHERE LOWER(email)=LOWER('$em') LIMIT 1";
			$result = mysqli_query($this->dbcon, $query); // the db query
			if(mysqli_num_rows($result) > 0)
				die('err9'); // email taken
			/* if the script didn't die until now, then all's okay */
		}

		/* the setters */
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
		public function setCaptcha($cid, $cresp) {
			$this->captcha['cid'] = trim($cid);
			$this->captcha['cresp'] = trim($cresp);
		}
		public function setData($fname, $lname, $username, $email, $pass) {
			$this->data['fname'] = trim($fname);
			$this->data['lname'] = trim($lname);
			$this->data['username'] = trim($username);
			$this->data['email'] = trim($email);
			$this->data['pass'] = trim($pass);
		}
		public function setAvailabilityData($username, $email) {
			$this->data['username'] = trim($username);
			$this->data['email'] = trim($email);
		}
	}
?>