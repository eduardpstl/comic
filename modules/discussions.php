<?php
	class Discussions {
		private $dbcon;
		public $err;
		/* and the methods */
		public function __construct() {
			$this->dbcon = null;
		}

		// 100
		public function postDiscussion($title = '', $category = 0, $tags = '', $post = '', $lang = 0, $session = null) {
			/* basic checks - nothing except the tags can be empty */
			$category = intval($category); $lang = intval($lang);
			if($title == '' || $category == 0 || $post == '' || $lang == 0 || !$session)
				$this->error('err100-1'); // empty parameter...
			if(!$this->dbcon)
				noDb();
			/* with the basic checks done, let's get to the other ones... */
			if(!is_numeric($category))
				$this->error('err100-2'); // invalid category
			if(!is_numeric($lang))
				$this->error('err100-3'); // invalid language
			if(!$session->isLoggedIn)
				$this->error('err100-4'); // user not logged in
			if(!preg_match('/[a-zA-Z0-9-]+(,[a-zA-Z0-9-]+)*/', $tags))
				$this->error('err100-5'); // the tags aren't valid
			/* let's see if the user can post - he can post only once every 30 seconds */
			$query = "SELECT last_post FROM accounts WHERE aid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('100-10'); // user cannot post...
			if(mysqli_fetch_assoc($result)['last_post'] + 30 >= time())
				$this->error('101-11'); // trying to post too much
			/* basic data in, let's retrieve the category and language identifiers... and the creator id */
			$query = "SELECT * FROM categories WHERE catid=$category LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('err100-6'); // the category is invalid
			$cat = mysqli_fetch_assoc($result);
			$cat_id = $cat['catid'];
			$cat_name = $cat['name'];
			$cat_description = $cat['description'];
			$cat_color = $cat['color'];
			$query = "SELECT * FROM languages WHERE lid=$lang LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('err100-7'); // the language identifier is invalid...
			$lang = mysqli_fetch_assoc($result);
			$lang_id = $lang['lid'];
			$lang_name = $lang['name'];
			$lang_sn = $lang['shortname'];
			$creatorid = $session->public_data['id'];
			$creator_username = $session->public_data['username'];
			$query = "SELECT * FROM accounts WHERE aid=$creatorid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('err100-8'); // unidentified creator... try logging in again
			/* after finding the identifiers, we continue with escaping the data we have... */
			$title = mysqli_real_escape_string($this->dbcon, htmlentities($title));
			$post = mysqli_real_escape_string($this->dbcon, htmlentities($post));
			$cat_description = mysqli_real_escape_string($this->dbcon, $cat_description);
			/* and we finally create the db entry */
			$time = time();
			$query = "UPDATE accounts SET last_post=$time WHERE aid=$creatorid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('err100-9'); // could not create...
			$query = "INSERT INTO discussions VALUES (null, '$title', '$tags', $cat_id, '$cat_name', '$cat_description', '$cat_color', $lang_id, '$lang_name', '$lang_sn', '$post', $creatorid, '$creator_username', $time, $time, null)";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('err100-9'); // couldn't create... try again
			die('done');
		}

		// 101
		public function postReply($post = '', $quoting = null, $threadid = 0, $session = null) {
			/* the basic checks */
			if(!$this->dbcon)
				noDb();
			/* the id */
			$threadid = $this->checkId($threadid, '101-1'); // invalid id
			/* the session */
			if(!$session || !isset($session->public_data))
				$this->error('101-2'); // invalid session
			if(!$session->isLoggedIn)
				$this->error('101-3'); // not logged in to do that
			if($quoting != null) 
				$quoting = $this->checkId($quoting, '101-4'); // invalid quote id
			if($post == '')
				$this->error('101-5'); // the post cannot be empty
			/* check to see if the thread exists, and if the user didn't post in the last 30 seconds... */
			$query = "SELECT did FROM discussions WHERE did=$threadid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('101-6'); // thread does not exist...
			$userid = $session->public_data['id'];
			$query = "SELECT last_post FROM accounts WHERE aid=$userid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('101-11'); // user cannot post...
			if(mysqli_fetch_assoc($result)['last_post'] + 30 >= time())
				$this->error('101-7'); // trying to post too much
			/* escape the actual post */
			$post = mysqli_real_escape_string($this->dbcon, htmlentities($post));
			/* first things first, we update what we need to update... the last the someone posted, and the update time of the thread */
			$time = time();
			$query = "UPDATE accounts SET last_post=$time WHERE aid=$userid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('101-8'); // could not post...
			$query = "UPDATE discussions SET time_updated=$time WHERE did=$threadid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('101-8');

			/* and the final, creation query */
			$query = "";
			$time = time();
			if($quoting == null) 
				$query = "INSERT INTO replies VALUES (null, '$post', $time, null, $threadid, $userid)";
			else
				$query = "INSERT INTO replies VALUES (null, '$post', $time, $quoting, $threadid, $userid)";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				$this->error('101-8'); // could not post - try again?

			die('done');
		}

		// 200
		public function getAllDiscussions($page = 1, $session = null) { // TODO : verificarea paginii...
			/* nothing to check really... */
			$limit = "LIMIT ".(($page-1)*20).",20"; 
			if(!$this->dbcon)
				noDb(); // we have no db connection...
			$query = "SELECT * FROM discussions ORDER BY time_updated ASC $limit";
			$results = mysqli_query($this->dbcon, $query);
			if(!$results)
				return null; // no posts or no more posts to load
			return $results; // that's about all // TODO : pun doar anumite chestii la returnare...
		}

		// 201
		public function getThread($page = 1, $threadid = 0, $session = null) { // TODO : verificarea paginii
			$limit = "LIMIT ".(($page-1)*20).",20";
			if(!$this->dbcon)
				noDb();
			$threadid = intval($threadid);
			$query = "SELECT * FROM replies WHERE threadid=$threadid $limit";
			$results = mysqli_query($this->dbcon, $query);
			if(!$results)
				return null; // no posts or no more posts to load
			$query = "SELECT * FROM discussions WHERE did=$threadid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return null; // thread could not be found?
			return array('posts' => $results, 'thread' => mysqli_fetch_assoc($result));
		}

		// 202
		public function getNumberOfThreads($categories = array(), $tags = array()) {
			if(!$this->dbcon)
				noDb();
			$nrcat = count($categories);
			$nrtag = count($tags);

			$oncat = "";
			$ontag = "";
			$query = "";

			if($nrcat) {
				/* escape the data first... */
				for($i = 0; $i < $nrcat; $i++)
					$categories[$i] = mysqli_real_escape_string($this->dbcon, $categories[$i]);
				$category = $categories[0];
				$oncat .= "catid=$category ";
				for($i = 1; $i < $nrcat; $i++) {
					$category = $categories[$i];
					$oncat .= "OR catid=$category ";
				}
				$oncat = trim($oncat);
			}
			if($nrtag) {
				/* escape the data here too */
				for($i = 0; $i < $nrtag; $i++)
					$tags[$i] = mysqli_real_escape_string($this->dbcon, $tags[$i]);
				$tag = $tags[0];
				$ontag .= "tags LIKE '%$tag%' ";
				for($i = 1; $i < $nrtag; $i++) {
					$tag = $tags[$i];
					$ontag .= "OR tags LIKE '%$tag%' ";
				}
				$ontag = trim($ontag);
			}

			if($nrcat && $nrtag)
				$query = "SELECT COUNT(did) AS 'nr' FROM discussions WHERE $oncat OR $ontag";
			else if($nrcat && !$nrtag)
				$query = "SELECT COUNT(did) AS 'nr' FROM discussions WHERE $oncat";
			else if(!$nrcat && $nrtag)
				$query = "SELECT COUNT(did) AS 'nr' FROM discussions WHERE $ontag";
			else
				$query = "SELECT COUNT(did) AS 'nr' FROM discussions";

			$result = mysqli_query($this->dbcon, $query);
			if(!mysqli_num_rows($result))
				$this->error('err202-1'); // could not get the total number of threads

			return mysqli_fetch_assoc($result)['nr'];
		}

		// 203
		public function getNumberOfPosts($threadid = 0) {
			if(!$this->dbcon)
				noDb();
			/* check the id */
			$this->checkId($threadid, 'err203-1'); // not a valid id
			/* go through the db */
			$query = "SELECT COUNT(postid) AS 'nr' FROM replies WHERE threadid=$threadid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result or !mysqli_num_rows($result))
				$this->error('err203-2'); // thread not found?
			return mysqli_fetch_assoc($result)['nr'];
		}

		// 300
		public function deleteThread($threadid = 0, $session = null) {
			if(!$this->dbcon)
				noDb();
			$ok = 0;
			/* we need to do a couple of checks before actually doing something on the db */
			/* first, we'll do the basic ones... */
			/* check the id */
			$threadid = $this->checkId($threadid, 'err300-1'); // not a valid id
			/* check the session */
			if(!$session || !isset($session->public_data))
				$this->error('err300-2'); // not a valid session
			/* check to see if the user is actually authorized to remove the thread - first, we'll check the user's who wants to do it access rank */
			$userid = $session->public_data['id'];
			if($session->public_data['rank'] == 2) // if true, he can remove it
				$ok = 1;
			/* if not, we check whether he is the creator and proceeded with the delete in 10 minutes */
			if(!$ok) {
				$query = "SELECT time_created,creatorid FROM discussions WHERE did=$threadid LIMIT 1";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result || !mysqli_num_rows($result))
					$this->error('err300-3'); // thread invalid...
				$data = mysqli_fetch_assoc($result);
				if($data['time_created'] + 10 * 60 >= time()) // the user can delete his thread even without moderator rights, within 10 minutes */
					if($data['creatorid'] == $userid) // only the creator can delete his threads (duh...)
						$ok = 1;
					else $this->error('err300-4'); // not the creator
				else $this->error('err300-5'); // ten minutes have passed since creation
			}

			if($ok) { // if everything's okay, we proceed with the deletion
				$query = "DELETE FROM discussions WHERE did=$threadid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					$this->error(mysqli_error($this->dbcon)); // could not delete... try again
			} else $this->error('err300-7'); // you're either not a moderator or the creator...

			die('done');
		}

		// 301
		public function deletePost($postid = 0, $session = null) {
			if(!$this->dbcon)
				noDb();
			$ok = 0;
			/* check the received ids */
			$postid = $this->checkId($postid, 'err301-1'); // invalid post id
			/* and the session */
			if(!$session || !isset($session->public_data))
				$this->error('err301-2'); // invalid session
			/* check to see if the user is actually authorized to remove the thread - he either is its creator or a moderator */
			$userid = $session->public_data['id'];
			if($session->public_data['rank'] == 2) // if true, he can remove it
				$ok = 1;
			if(!$ok) {
				$query = "SELECT creatorid FROM replies WHERE postid=$postid LIMIT 1";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result || !mysqli_num_rows($result))
					$this->error('err301-4'); // post id incorrect
				$data = mysqli_fetch_assoc($result);
				if($data['creatorid'] == $userid)
					$ok = 1;
				else $this->error('err301-5'); // you're not its creator
			}
			/* and finally, the update */
			if($ok) {
				$query = "DELETE FROM replies WHERE postid=$postid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					$this->error('err301-6'); // could not delete; try again
			} else $this->error('err301-7'); // you're either not the creator or a moderator

			die('done');
		}

		// 400
		public function editThread($threadid = 0, $toedit = array(), $session = null) {
			if(!$this->dbcon)
				noDb();
			/* check the id */
			$threadid = $this->checkId($threadid, 'err400-1'); // invalid thread id...
			/* the session */
			if(!$session || !isset($session->public_data))
				$this->error('err400-2'); // invalid session or not logged in...
			/* check whether the thread exists or not */
			$query = "SELECT * FROM discussions WHERE did=$threadid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('err400-3'); // thread not found
			$threaddata = mysqli_fetch_assoc($result);
			$userid = $session->public_data['id'];
			/* see if the user has the rights to edit the thread */
			if($threaddata['creatorid'] != $userid && $session->public_data['rank'] != 2) // he's the creator or a moderator, so he can edit it
				$this->error('err400-4'); // not the creator or a moderator
			/* now, if we have the rights and know the thread exists, we can update the stuff, if there's anything to update */
			if(count($toedit)) {
				$query = "UPDATE discussions SET";
				$edited = 0;

				if(isset($toedit['title']) && strcmp($toedit['title'], $threaddata['title'])) {
					$title = mysqli_real_escape_string($this->dbcon, htmlentities($toedit['title']));
					$query .= " title='$title',"; $edited++;
				}
				if(isset($toedit['tags']) && strcmp($toedit['tags'], $threaddata['tags'])) {
					$tags = $toedit['tags'];
					if(!preg_match('/[a-zA-Z0-9-]+(,[a-zA-Z0-9-]+)*/', $tags))
						$this->error('err-400-10'); // those don't look like tags should...
					$query .= " tags='$tags',"; $edited++;
				}
				if(isset($toedit['category']) && strcmp($toedit['category'], $threaddata['category'])) {
					$category = $toedit['category'];
					$category = $this->checkId($category, 'err400-11'); // that doesn't look like a category
					$temp = "SELECT * FROM categories WHERE catid=$category LIMIT 1";
					$result = mysqli_query($this->dbcon, $temp);
					if(!$result || !mysqli_num_rows($result))
						$this->error('err400-12'); // category not found... <sarcasm> I wonder why </sarcasm>
					$cats = mysqli_fetch_assoc($result);
					$cat_d = mysqli_real_escape_string($this->dbcon, $cats['description']);
					$cat_n = mysqli_real_escape_string($this->dbcon, $cats['name']);
					$cat_c = mysqli_real_escape_string($this->dbcon, $cats['color']);
					$query .= " catid=$category,cat_name='$cat_n',cat_description='$cat_d',cat_color='$cat_c',"; $edited++;
				}
				if(isset($toedit['lid']) && strcmp($toedit['lid'], $threaddata['lid'])) {
					$lid = $toedit['lid'];
					$lid = $this->checkId($lid, 'err400-13'); // that doesn't look like a language...
					$temp = "SELECT * FROM languages WHERE lid=$lid LIMIT 1";
					$result = mysqli_query($this->dbcon, $temp);
					if(!$result || !mysqli_num_rows($result))
						$this->error('err400-14'); // language not found...
					$langs = mysqli_fetch_assoc($result);
					$lang_n = mysqli_real_escape_string($this->dbcon, $langs['name']);
					$lang_sn = mysqli_real_escape_string($this->dbcon, $langs['shortname']);
					$query .= " lid=$lid,lang_name='$lang_n',lang_shortname='$lang_sn',"; $edited++;
				}
				if(isset($toedit['post']) && strcmp($toedit['post'], $threaddata['post'])) {
					$post = $toedit['post'];
					$post = mysqli_real_escape_string($this->dbcon, htmlentities($post));
					$query .= " post='$post',"; $edited++;
				}

				if($edited) {
					$time = time();
					$query .= " time_edited=$time WHERE did=$threadid";

					$result = mysqli_query($this->dbcon, $query);
					if(!$result)
						$this->error(mysqli_error($this->dbcon)); // could not update
				} else $this->error('err400-5');

			} else $this->error('err400-5'); // nothing to edit

			die('done');
		}


		// 401
		public function editPost($postid = 0, $toedit = array(), $session = null) {
			if(!$this->dbcon)
				noDb();
			/* like always... the id... */
			$postid = $this->checkId($postid, 'err401-1'); // invalid id
			/* the session... */
			if(!$session || !isset($session->public_data))
				$this->error('err401-2'); // invalid session or not logged in...
			/* see if the post exists */
			$query = "SELECT * FROM replies WHERE postid=$postid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				$this->error('err401-3'); // post not found - where did you get the id then?!?!
			$postdata = mysqli_fetch_assoc($result);
			/* and we enter the modification if */
			if(count($toedit)) {
				if(isset($toedit['post'])) {
					$post = htmlentities($toedit['post']);
					if(!strcmp($post, $postdata['post']))
						$this->error('err401-4');

					$post = mysqli_real_escape_string($this->dbcon, $post);
					$time = time();

					$query = "UPDATE replies SET post='$post',time_edited=$time WHERE postid=$postid";
					$result = mysqli_query($this->dbcon, $query);
					if(!$result)
						$this->error('err401-5'); // could not edit...
				}
			} else $this->error('err401-4'); // nothing modified

			die('done');
		}

		private function checkId($threadid, $msg) {
			if(!preg_match('/[0-9]+/', $threadid))
				$this->error($msg);
			$threadid = intval($threadid);
			if(!is_numeric($threadid))
				$this->error($msg);
			if(!$threadid)
				$this->error($msg);
			return $threadid;
		}

		private function noDb() {
			// do something...
			die('err001');
		}
		private function error($msg) {
			die($msg);
		}
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}

	$discussions = new Discussions();
	include_once 'dbcon.php';
	$discussions->setDbCon($dbcon);
?>