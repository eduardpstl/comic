<?php
	class Groups {
		private $dbcon;
		private $dbdata = array();
		public $joined;
		public $club = array();
		public $errmsg;

		public function __construct() {
			$this->dbcon = null;
			$this->dbdata = array('cid' => 0, 'cname'=> "", 'cshortname' => "", 'cdesc' => "", 'chash' => "", 'cnrmembers' => 0, 'ccategory' => "", 'ctags' => "", 'ccreated' => 0, 'privacy' => "");
			$this->group = array('id' => 0, 'name'=> "", 'shortname' => "", 'desc' => "", 'members' => array());
			$this->joined = false;
			$this->errmsg = "";
		}

		public function loadData($cid = 0, $cname = "", $loggedin = false, $uid = 0) {
			$which = 0; // var to store with which thing we're searching the club with
			if($cid == 0 && $cname == "" && $uid == 0)
				return $this->error('err11'); // nothing to parse...
			if($cid == "" && $cname == "" && $uid == "")
				return $this->error('err11');
			if(!$this->dbcon) die('err001'); // could not connect to the db
			/* we check to see if the data is in the correct format */
			$id_patt = '/[0-9]+/'; // basic id patt...
			$name_patt = '/[a-zA-Z0-9.-]+/'; // we can have anything as a club name
			/* and now we check the patterns, after we escape the data */
			if(strlen($cid) > 0) $which = 1; // the club id
			else if(strlen($cname) > 0) $which = 2; // by the club shortname
			else return $this->error('err15'); // some error occured
			$query = "";
			if($which == 1) {
				$cid = mysqli_real_escape_string($this->dbcon, $cid);
				if(!preg_match($id_patt, $cid))
					return $this->error('err12'); // club id or club name not correct
				$cid = intval($cid);
				$query = "SELECT * FROM groups WHERE cid=$cid LIMIT 1";
			} else if($which == 2) {
				$cname = mysqli_real_escape_string($this->dbcon, $cname);
				if(!preg_match($name_patt, $cname))
					return $this->error('err12');
				$query = "SELECT * FROM groups WHERE LOWER(TRIM(cshortname))='$cname' LIMIT 1";
			} else
				return $this->error('err15');
			/* if the basic checks are all right, we can continue by loading the data from the db */
			$results = mysqli_query($this->dbcon, $query);
			if(!mysqli_num_rows($results))
				return $this->error('err13'); // the requested group wasn't found
			$this->dbdata = mysqli_fetch_assoc($results);
			/* load the users who joined the group */
			$file = @file_get_contents('../assets/caches/groups/'.$this->dbdata['chash']);
			if(!$file)
				return $this->error('err15');
			/* unserialize it to make it readable , and get the json data from it */
			$us = unserialize($file);
			$members = json_decode($us, true);
			/* put the data for display */
			$this->group['id'] = $this->dbdata['cid'];
			$this->group['name'] = $this->dbdata['cname'];
			$this->group['shortname'] = $this->dbdata['cshortname'];
			$this->group['desc'] = $this->dbdata['cdesc'];
			$this->group['members'] = $members;
			/* finally, if we have a logged in user, check if he is in the group or not */
			// die(var_dump($this->group));
			if($loggedin == true && $this->hasJoined($this->group['members'], $uid))
				$this->joined = true;
		}

		public function addGroup($name = "", $shortname = "", $description = "", $tags = "", $category = "", $privacy = "", $creator_id = 0) {
			/* check the db first */
			if(!$this->dbcon) die('err001');
			/* first, we escape and verify the data */
			if($name == "" || $shortname == "" || $description == "" || $category == "" || $privacy == "" || $creator_id == 0)
				return $this->error('err21'); // missing a parameter
			/* check reserved words */
			if(!strcmp($shortname, 'create') || !strcmp($shortname, 'edit')) 
				return $this->error('err20'); // using reserved words...
			/* we convert everything to a html entity, with the exception of the short name, tags and category, which we already know how they should look */
			$shortname = mysqli_real_escape_string($this->dbcon, $shortname);
			$tags = mysqli_real_escape_string($this->dbcon, $tags);
			$category = mysqli_real_escape_string($this->dbcon, $category);
			$privacy = mysqli_real_escape_string($this->dbcon, $privacy);
			$name = htmlentities($name);
			$description = htmlentities($description);
			$sn_patt = '/[a-zA-Z0-9.-]+/';
			$tags_patt = '/[a-zA-Z0-9-]+(,[a-zA-Z0-9-]+)*/';
			$cat_patt = '/[a-zA-Z0-9]+/';
			if(!preg_match($sn_patt, $shortname))
				return $this->error('err22'); // the shortname doesn't look how it should
			if($tags != "" && !preg_match($tags_patt, $tags))
				return $this->error('err23'); // the tags don't look how they should
			if(!preg_match($cat_patt, $category))
				return $this->error('err24'); // the category doesn't look like how it should
			if(!preg_match($cat_patt, $privacy))
				return $this->error('err25'); // the privacy doesn't look like how it should
			if(!is_numeric($creator_id))
				return $this->error('err26'); // the creator id doesn't look like how it should
			/* preliminary checks done, now we do the db checks - we must not have two groups with the same name or shortname */
			$query = "SELECT * FROM groups WHERE LOWER(TRIM(cname)) = LOWER('$name') OR LOWER(TRIM(cshortname)) = LOWER('$shortname') LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(mysqli_num_rows($result))
				return $this->error('err27'); // club name or club shortname already taken
			/* before creating the actual db entry, let's check its creator */
			$query = "SELECT username FROM accounts WHERE aid=$creator_id LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!mysqli_num_rows($result))
				return $this->error('err28'); // the creator is invalid
			$username = mysqli_fetch_assoc($result)['username'];
			/* now that everything's okay, we proceed with the club creation */
			$time = time();
			$hash = sha1($shortname);
			$query = "INSERT INTO groups (cname,cshortname,cdesc,chash,cnrmembers,ccategory,ctags,ccreated,privacy)
								VALUES ('$name','$shortname','$description','$hash',1,'$category','$tags',$time,'$privacy')";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result) 
				return $this->error('err29'); // an error happened...
			/* and create the cache */
			$some_json = json_encode(array(array('username' => $username, 'id' => intval($creator_id), 'rank' => 4)));
			$sl = serialize($some_json);
			@file_put_contents('../assets/caches/groups/'.$hash, $sl);
			/* add the group to the creator's group list */
			$someid = mysqli_insert_id($this->dbcon);
			if(!is_numeric($someid) && $someid == 0) return $this->error('err29');
			include_once 'helper.php';
			updateJson('../assets/caches/users/'.sha1($creator_id), array('cid' => $someid, 'cname' => $name, 'rank' => 4));
			/* done ? */
			die('done');
		}

		public function editDetails($edits = array(), $original = array()) {
			/*checks... and checks */
			if(!$this->dbcon) die('err001'); // no db connection...
			$toedit = array();
			$toeditwith = array();
			$errmsg = array();
			/* we go through each member and see if there's anything to edit, and escape and check the data while we're at it */
			if(isset($edits['cname'])) {
				$edits['cname'] = htmlentities($edits['cname']);
				if(!strcmp($edits['cname'], $original['cname'])) {
					$toedit[count($toedit)] = 'cname';
					$toeditwith[count($toeditwith)] = mysqli_real_escape_string($this->dbcon, $edits['cname']);
					$errmsg['cname'] = 'ok';
				} else $errmsg['cname'] = 'err01'; // club name same as before
			} else $errmsg['cname'] = 'err00'; // club name not changed
			if(isset($edits['cshortname'])) {
				if(!strcmp($edits['cshortname'], $original['cshortname'])) {
					$toedit[count($toedit)] = 'cshortname';
					$sn_patt = '/[a-zA-Z0-9.-]+/';
					$temp = mysqli_real_escape_string($this->dbcon, $edits['cname']);
					if(!preg_match($sn_patt, $temp))
						return $this->error('err31'); // invalid short name
					$toeditwith[count($toeditwith)] = $temp;
					$errmsg['cshortname'] = 'ok';
				} else $errmsg['cshortname'] = 'err11'; // short name not changed
			} else $errmsg['cshortname'] = 'err10'; // short name not changed
			if(isset($edits['cdesc'])) {
				$edits['cdesc'] = htmlentities($edits['cdesc']);
				if(!strcmp($edits['cdesc'], $original['cdesc'])) {
					$toedit[count($toedit)] = 'cdesc';
					$toeditwith[count($toeditwith)] = mysqli_real_escape_string($this->dbcon, $edits['cdesc']);
					$errmsg['cdesc'] = 'ok';
				} else $errmsg['cdesc'] = 'err21'; // description same as before
			} else $errmsg['cdesc'] = 'err20'; // description not changed
			/* check in the db to see if we already have the name or short name, if it changed, of course */
			if(!in_array('ok', $errmsg))
				return $this->error('err32'); // nothing to change
			/* check the db to see if we have anything to change */
			if(!strcmp($errmsg['cname'], 'ok') || !strcmp($errmsg['cshortname'], 'ok')) {
				$name = $toeditwith['cname'];
				$sname = $toeditwith['cshortname'];
				$query = "SELECT * FROM groups WHERE LOWER(TRIM(cname))=LOWER('$name') OR LOWER(TRIM(cshortname))=LOWER('$sname') LIMIT 1";
				$result = mysqli_query($this->dbcon, $query);
				if(mysqli_num_rows($result))
					return $this->error('err33'); // name or short name already taken
			}
			/* and we finally do the update */
			$id = $original['cid'];
			$query = "UPDATE groups WHERE cid=$id SET";
			if(!strcmp($errmsg['cname'], 'ok')) {
				$newname = $toeditwith['cname'];
				$query .= " cname='$newname'";
				if(!strcmp($errmsg['cname'], 'ok') || !strcmp($errmsg['cdesc'], 'ok'))
					$query .= ",";
			}
			if(!strcmp($errmsg['cshortname'], 'ok')) {
				$newshortname = $toeditwith['cshortname'];
				$query .= " cshortname='$newshortname'";
				if(!strcmp($errmsg['cdesc'], 'ok'))
					$query .= ",";
			}
			if(!strcmp($errmsg['cdesc'], 'ok')) {
				$newdesc = $toeditwith['cdesc'];
				$query .= " cdesc='$newdesc'";
			}
			/* and execute the update */
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return $this->error('err34'); // some error ocured...
			/* and done */
		}

		public function promoteUser($name = '', $clubid = 0, $userid = 0, $rank = 0) {
			/* basic checks, although they should be fine... */
			if(!is_numeric($userid))
				return $this->error('err42'); // user id not valid
			if(!is_numeric($rank))
				return $this->error('err43'); // rank not valid
			if($userid == 0 || $rank == 0) 
				return $this->error('err44'); // user id or rank not valid
			$sn_patt = '/[a-zA-Z0-9.-]+/';
			if(!preg_match($sn_patt, $name))
				return $this->error('err41'); // club id not valid
			/* now that we have the basic checks, we'll load the caches */
			$fc = @file_get_contents('../assets/cache/groups/'.sha1($name));
			if(!$fc) return $this->error('err45'); // the club data could not be loaded; try again
			$fu = @file_get_contents('../assets/cache/users/'.sha1($userid));
			if(!$fu) return $this->error('err45');
			/* unserialize and unjson them */
			$uc = unserialize($fc);
			$uu = unserialize($fu);
			$jc = json_decode($fc, true);
			$ju = json_decode($fu, true);
			/* find the respective ids */
			$idc = $this->getArrId($jc, $userid, 'id');
			$idu = $this->getArrId($ju, $clubid, 'cid');
			/* we do a preliminary rank check... */
			if(!is_numeric($rank) && ($rank != 0 || 3 || 4))
				return $this->error('err46'); // the rank doesn't look like how it should
			/* and update */
			$jc[$idc]['id'] = $rank;
			$ju[$idu]['cid'] = $rank;
			/* and we put everything back */
			$jc = json_encode($jc);
			$ju = json_encode($ju);
			$sc = serialize($jc);
			$su = serialize($ju);
			$fc = @file_get_contents('../assets/cache/groups/'.sha1($name), $sc);
			if(!$fc) return $this->error('err45');
			$fu = @file_put_contents('../assets/cache/users/'.sha1($userid), $su);
			if(!$fu) return $this->error('err45');
		}

		private function error($msg = '') {
			$this->errmsg = $msg;
			die($msg);
			return false;
		}

		private function hasJoined($arr = array(), $uid = 0) {
			foreach($arr as $member)
				if($member['id'] == $uid) return true;
			return false; 
		}

		private function getArrId($arr = array(), $uid = 0, $what = 'id') {
			for($i = 0, $ii = count($arr); $i < $ii; $i++)
				if(isset($arr[$i][$what]) && $arr[$i][$what] == $uid)
					return $i;
			return -1;
		}

		/* setters */
		public function setDbCon($con = null) {
			$this->dbcon = $con;
		}
	}

	$groups = new Groups();
	@include_once 'dbcon.php';
	$groups->setDbCon($dbcon);
?>