<?php
	class Feed {
		private $dbcon;
		public $errmsg;

		public function __construct() {
			$this->dbcon = null;
		}

		public function updateComment($userid = 0, $message = '', $quote = null, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* check the ids */
			$userid = $this->isNumber($userid); // is not a number
			if($quote) $quote = $this->isNumber($quote);
			if(!$userid || !$creatorid)
				return $this->error('err1');
			/* session */
			if(!$session || !isset($session->public_data))
				return $this->error('err2'); // session not set?
			/* message */
			if(strlen($message) < 2)
				return $this->error('err3'); // message not of at least 2 letters
			/* escape what we need to escape */
			$message = mysqli_real_escape_string($this->dbcon, htmlentities($message));
			/* if everything is okay, we can proceed with updating the database */
			$query = ''; $time = time();
			if($quote)
				$query = "INSERT INTO feeds VALUES (null, $userid, $creatorid,$quote,'$message',$time)";
			else
				$query = "INSERT INTO feeds VALUES (null, $userid, $creatorid,null,'$message',$time)";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return $this->error('err4'); // could not update?
		}

		public function getComments($userid = 0, $page = 1) {
			if(!$this->dbcon)
				$this->noDb();
			/* id and page */
			$userdid = $this->isNumber($userid); // not a number
			$page = $this->isNumber($page);
			if($userid == null || $page == null)
				return $this->error('err1');
			/* and the query */
			$limit = "LIMIT ".(($page-1)*15).",15";
			$query = "SELECT * FROM feeds WHERE userid=$userid $limit";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err6'); // could not load anymore comments?
			return json_encode($result);
		}

		public function getNumberOfComments($userid = 0) {
			if(!$this->dbcon)
				$this->noDb();
			/* id */
			$userid = $this->isNumber($userid);
			if($userid == null)
				return $this->error('err1'); // not a number
			/* and the query */
			$query = "SELECT COUNT(updateid) AS 'count' FROM feeds WHERE userid=$userid";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err7'); // could not get number of updates
			return mysqli_fetch_assoc($result)['cound'];
		}

		private function isNumber($number) {
			if(!preg_match('/[0-9]+/', $number))
				return null;
			$number = intval($number);
			if(!is_numeric($number))
				return null;
			if(!$number)
				return null;
			return $number;
		}

		private function noDb() {
			die('err001');
		}
		private function error($msg) {
			$this->errmsg = $msg;
			return null;
		}
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}

	$feed = new Feed();
	@include_once 'dbcon.php';
	$feed->setDbCon($dbcon);
?>