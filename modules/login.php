<?php
	session_start();

	class LogIn {
		private $dbdata = array();
		private $data = array();
		private $captcha = array();
		private $remember_me;
		private $dbcon;
		private $which;

		public function __construct() {
			$this->data = array('logInData' => "", 'pass' => "");
			$this->captcha = array('cid' => "", 'cresp' => "");
			$this->dbcon = null;
			$this->remember_me = false;
			$this->which = 0;
		}
		/* same as in sign up's case, a method to do it all */
		public function doLogIn() {
			if(!$this->dbcon) die('err001'); // could not connect to the db
			$this->escapeData();
			$this->checkCaptcha(); // if necessary
			$this->checkUsernameOrEmail();
			$this->checkPassword();
			$this->updateDatabase();
			$this->createSession();
		}
		/* escaping the data... besides the username, we don't have much to do */
		private function escapeData() {
			/* the small checks */
			if($this->data['logInData'] == '') // one of them can be empty, cause we're logging in the user only through one
				die('err3'); // username, email or pass cannot be empty
			if($this->data['pass'] == '')
				die('err3');
			/* escape the username if it's not empty, and we're done here */
			//if($this->username != '') $this->data['username'] = mysqli_real_escape_string($this->dbcon, $this->data['username']);
		}
		/* the method which checks the captcha if there's the case (after 5 consecutive log in fails) */
		private function checkCaptcha() {
			if(isset($_SESSION['numfails']) && $_SESSION['numfails'] > 5) {
				/* code from the recaptcha php documentation - https://developers.google.com/recaptcha/docs/php */
				require_once('recaptcha/recaptchalib.php');
				require_once('recaptcha/recaptchakeys.php');
				$resp = recaptcha_check_answer ($privatere_key,
				                          $_SERVER["REMOTE_ADDR"],
				                          $this->captcha['cid'],
				                          $this->captcha['cresp']);

				if (!$resp->is_valid)
					die('err2'); // invalid captcha response
			}
		}
		/* now we're checking the username or email (to see if they exist) and load the database variables as well */
		private function checkUsernameOrEmail() {
			/* some small variables to help us a bit later */
			$what = '';
			$login_var = '';
			/* first things first, let's see if we have an username or an email - we check them through this as well */
			$username_patt = '/^[a-zA-Z][a-zA-Z0-9\\-_]{2,20}/';
			$email_patt = '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/'; // standard email regex, found on stackoverflow - http://stackoverflow.com/questions/201323/using-a-regular-expression-to-validate-an-email-address/1917982#1917982
			if(preg_match($email_patt, $this->data['logInData'])) {
				// we have an email
				$what = 'email';
				$this->which = 2; // email
				$login_var = $this->data['logInData'];
			} else if(!preg_match($username_patt, $this->data['logInData'])) {
				// we have an username
				$what = 'username';
				$this->which = 1; // username
				$login_var = $this->data['logInData'];
			} else die('err4'); // the log in variable didn't match either an username or email
			/* if we got this far, then we can search through the db */
			$query = "SELECT * FROM accounts WHERE LOWER($what)=LOWER('$login_var') LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(mysqli_num_rows($result)) { // if we do have an entry
				$this->dbdata = mysqli_fetch_assoc($result);
				/* and now we have the db data, but if we don't ...*/
			} else if($this->which == 1) die('err5'); // invalid username or password
			else if($this->which == 2) die('err6mail'); // invalid email or password
			else die('err8'); // some error was encountered
		}
		/* the method which checks the password - nothing much to say about it */
		private function checkPassword() {
			/* basically we do exactly what we did at sign up - split, reverse, put salt and username in the middle, and hash */
			$half = (int)(strlen($this->data['pass']) / 2);
			$first_h = substr($this->data['pass'], 0, $half);
			$second_h = substr($this->data['pass'], $half);
			$hash = hash('sha512', $second_h.$this->dbdata['salt'].$this->dbdata['username'].$first_h);
			/* if the two hashes (the one from the db and this one) are equal, then the password is legit */
			if($hash != $this->dbdata['password']) {
				if($this->which == 1) die('err5');
				else if($this->which == 2) die('err6pass');
				else die('err8');
			}
			/* we have a good pass, so we continue with updating the db */
		}
		/* we update the last log in timer now */
		private function updateDatabase() {
			$time = time();
			$id = $this->dbdata['aid'];
			$query = "UPDATE accounts SET last_login=$time WHERE aid=$id";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				die('err8');
			$this->dbdata['last_login'] = $time;
		}
		/* now we can finally create the session - which is a cookie */
		private function createSession() {
			/* the main, session id cookie will be:
			- after checking keep me logged in: password+md5(username)+last_login+useragent+'some_random_text'+/+AES256(random_string[12]+aid+time()) */
			$browser = '';
			if(isset($_SERVER['HTTP_USER_AGENT'])) $browser = $_SERVER['HTTP_USER_AGENT'];
			/* the first part is just a hashed mumble jumble of string */
			$cookie_hash = hash('sha512',$this->dbdata['password'].md5($this->dbdata['username']).$this->dbdata['last_login'].$browser.'some_random_text');
    		/* and now, that we have both the parts generated, let's put them into a cookie, if the user chose to remember him or into a session variable otherwise */
    		if($this->remember_me == false)
    			$_SESSION['id'] = $cookie_hash.'-'.$this->dbdata['aid'];
    		else
    			setcookie('id', $cookie_hash.'-'.$this->dbdata['aid'], time()+3600*24*31, '/'); // available for a month, to be sure
    		/* done */
    		die('allokay');
		}

		/* setters */
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
		public function setCaptcha($cid, $cresp) {
			$this->captcha['cid'] = trim($cid);
			$this->captcha['cresp'] = trim($cresp);
		}
		public function shouldKeepLoggedIn($isIt) {
			$this->remember_me = $isIt;
		}
		public function setData($logInData, $pass) {
			$this->data['logInData'] = trim($logInData);
			$this->data['pass'] = trim($pass);
		}

	}