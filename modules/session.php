<?php
	session_start();

	class Session {
		private $dbdata = array();
		private $dbcon;
		private $session_vars = array();
		public $isLoggedIn = false;
		public $public_data = array();
		public $err_message;

		public function __construct() {
			$this->dbcon = null;
			$this->isLoggedIn = false;
			$this->err_message = "";
			$this->public_data = array('username' => "", 'email' => "", 'id' => 0, 'rank' => 0);
			$this->session_vars = array('hash' => "", 'id' => 0);
		}
		/* same as in the other session related classes, just with a tinge of 'falseness' 
		   we can't have the script die, so we'll return a 'false' in case of an invalid session */
		public function doCheckSession() {
			if(!$this->dbcon) die('err001'); // could not connect to the db
			$isGood = $this->loadVariables();
			if($isGood) $isGood = $this->loadDbData();
			if($isGood) $this->checkVariables();
		}
		/* the first method - this tries to find any session variable */
		private function loadVariables() {
			/* first check */
			if(!isset($_SESSION['id']) && !isset($_COOKIE['id'])) {
				$this->err_message = "err1";
				return false; // no session var detected
			}
			$ses = "";
			if(isset($_SESSION['id']))
				$ses = $_SESSION['id'];
			else if(isset($_COOKIE['id']))
					$ses = $_COOKIE['id'];
			else { 
				$this->err_message = "err5";
				return false; // some kind of error?
			}
			/* now that we have the variable, we can start working with it - by splitting it into two */
			$vars = explode("-", $ses);
			/* the first should be the hash - let's check to see if it is so */
			if(isset($vars[0])) {
				$patt = '/[a-f0-9]+/';
				if(preg_match($patt, $vars[0]) == 0 || false) 
					return $this->notValid('err2'); // the cookie data is invalid
			} else return $this->notValid('err5'); 
			/* and we do the same with the id */
			if(isset($vars[1])) {
				$patt = '/[0-9]{1,14}/';
				if(preg_match($patt, $vars[1]) == 0 || false)
					return $this->notValid('err2');
			} else return $this->notValid('err5');
			if(isset($vars[2])) // some minor check - this shouldn't be true, and yeah, we aren't accessing it, but still!
				return $this->notValid('err2');
			/* now that we checked we have a proper hash and id, we'll put them into our var which should hold them for future use */
			$this->session_vars['hash'] = $vars[0];
			$this->session_vars['id'] = intval($vars[1]);
			if($this->session_vars['id'] == 0)
				return $this->notValid('err5');

			// the true return
			return true;
		}
		/* a quick method to load up all the dbdata */
		private function loadDbData() {
			/* nothing fancy - just a simple query */
			$id = $this->session_vars['id'];
			$query = "SELECT * FROM accounts WHERE aid=$id LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(mysqli_num_rows($result)) {
				/* found someone */
				$this->dbdata = mysqli_fetch_assoc($result);
			} else return $this->notValid('err2'); // we didn't find anyone, so the id is no good

			return true;
		}
		/* and the method which actually checks the hash with the db one */
		private function checkVariables() {
			/* first, we construct the 'cookie hash' */
			$browser = '';
			if(isset($_SERVER['HTTP_USER_AGENT'])) $browser = $_SERVER['HTTP_USER_AGENT'];
			$cookie_hash = hash('sha512',$this->dbdata['password'].md5($this->dbdata['username']).$this->dbdata['last_login'].$browser.'some_random_text');
			/* then we compare it */
			if($cookie_hash != $this->session_vars['hash'])
				return $this->notValid('err3'); // session invalid or expired
			/* if it's equal, we update the rest of our vars - we have a valid session */
			$this->isLoggedIn = true;
			$this->public_data['username'] = $this->dbdata['username'];
			$this->public_data['email'] = $this->dbdata['email'];
			$this->public_data['id'] = $this->dbdata['aid'];
			$this->public_data['rank'] = $this->dbdata['rank'];
			$this->public_data['first_time'] = $this->dbdata['first_time'];

			return true;
		}

		private function notValid($msg) {
			$this->err_message = $msg;
			return false;
		}

		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}

	$session = new Session();
	include_once 'dbcon.php';
	$session->setDbCon($dbcon);
?>