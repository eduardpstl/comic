<?php
	class Categories {
		private $dbcon;

		public function __construct() {
			$this->dbcon = null;
		}

		public function proposeCategory($description = '', $name = '', $color = '', $session) {
			if(!$this->dbcon)
				$this->noDb();
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err1'); // not logged in - only logged in users can do this...
			/* check the color */
			if(!preg_match('/#[0-9A-Za-z]{6,6}/', $color))
				return $this->error('err3'); // that doesn't look like a color
			/* escape the other data */
			$description = mysqli_real_escape_string($this->dbcon, htmlentities($description));
			$name = mysqli_real_escape_string($this->dbcon, htmlentities($name));
			/* and prepare the query... */
			$time = time(); $userid = $session->public_data['id'];
			$query = "INSERT INTO pending_categories VALUES (null,'$description','$name','$color',$userid,$time)";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return $this->error('err4'); // could not end the action; try again?
			$pcatid = mysqli_insert_id($this->dbcon);
			/* add this to the user's contributions */
			$query = "INSERT INTO user_contributions VALUES (null,$userid,$time,$pcatid,3,0)"; // contribution id, user id, time, the 'link' to whatever the user did, 3 - proposed a category, 0 - pending
			$result = mysqli_query($this->dbcon, $query);
			if(!$result)
				return $this->error('err4');
		}

		public function acceptOrDenyPending($pcatid = '', $resp = null, $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* the id... */
			$pcatid = $this->isNumber($pcatid);
			if($pcatid == null)
				return $this->error('err2'); // not a number
			/* the response */
			if($resp != true || false)
				return $this->error('err7'); // doesn't look like a proper reply...
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err1'); // not logged in
			/* check the rank */
			if($session->public_data['rank'] < 3) // only db moderators can approve things
				return $this->error('err5'); // you don't have the rank?
			/* and check if the proposed category still exists */
			$query = "SELECT * FROM pending_categories WHERE pcatid=$pcatid LIMIT 1";
			$result = mysqli_query($this->dbcon, $query);
			if(!$result || !mysqli_num_rows($result))
				return $this->error('err6'); // didn't find the pending cat...
			/* prepare the data.. */
			$data = mysqli_fetch_assoc($result); 
			$desc = mysqli_real_escape_string($data['description']);
			$name = mysqli_real_escape_string($data['name']);
			$userid = mysqli_real_escape_string($data['userid']);
			$color = $data['color'];
			/* and finally, we do the last queries */
			if($resp == true) { // the category was accepted
				/* first, add the new category */
				$query = "INSERT INTO categories VALUES (null,'$desc','$name','$color')";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8'); // couldn't add the new category
				/* next, we update the status on the user's contribution */
				$query = "UPDATE user_contributions SET status=1 WHERE userid=$userid"; // 1 - accepted
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8');
				/* add experience to the user */
				$query = "UPDATE accounts SET exp=exp+2 WHERE aid=$userid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8');
				/* and finally, remove the pending request... */
				$query = "DELETE pending_categories WHERE pcatid=$pcatid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8');
				return 'done';
			} else if($resp == false) {
				/* update the status on the user's contribution */
				$query = "UPDATE user_contributions SET status=2 WHERE userid=$userid"; // 2 - denied
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8');
				/* finally, remove the pending request... */
				$query = "DELETE pending_categories WHERE pcatid=$pcatid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err8');
				return 'done';
			} else return $this->error('err7');
		}

		public function editCategory($catid = 0, $toedit = array(), $session = null) {
			if(!$this->dbcon)
				$this->noDb();
			/* id */
			$catid = $this->isNumber($catid);
			if($catid == null)
				return $this->error('err2'); // not a valid id...
			/* the session */
			if(!$session || !isset($session->public_data))
				return $this->error('err1'); // not a valid session
			/* the rank */
			if($session->rank < 3)
				return $this->error('err5'); // not enough privileges
			/* finally, we build the query */
			if(count($toedit)) {
				$query = "UPDATE categories SET ";
				if(isset($toedit['description']) && count($toedit) > 1)
					$query .= "description='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['description']))."', ";
				else if(isset($toedit['description']))
					$query .= "description='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['description']))."' ";
				if(isset($toedit['name']) && isset($toedit['color']))
					$query .= "name='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['name']))."', ";
				else if(isset($toedit['name']))
					$query .= "name='".mysqli_real_escape_string($this->dbcon, htmlentities($toedit['name']))."' ";
				if(isset($toedit['color']) && preg_match('/#[0-9A-Za-z]{6,6}/', $toedit['color']))
					$query .= "color='".$toedit['color']."' ";
				else return $this->error('err3'); // doesn't look like a color...

				$query .= "WHERE catid=$catid";
				$result = mysqli_query($this->dbcon, $query);
				if(!$result)
					return $this->error('err9'); // couldn't edit... who knows why
			} else return $this->error('err10'); // nothing to edit...
		}

		private function isNumber($number) {
			if(!preg_match('/[0-9]+/', $number))
				return null;
			$number = intval($number);
			if(!is_numeric($number))
				return null;
			if(!$number)
				return null;
			return $number;
		}

		private function noDb() {
			die('err001');
		}
		private function error($msg) {
			$this->errmsg = $msg;
			return null;
		}
		public function setDbCon($con) {
			$this->dbcon = $con;
		}
	}
?>