<?php
	class GetProfileData {
		public $profile;
		public $errmsg;
		private $data;
		private $dbcon;
		private $which;
		public $found;

		public function __construct() {
			$this->profile = array('fname' => "", 'lname' => "", 'username' => "", 'id' => "");
			$this->dbcon = null;
			$this->which = 0; // 1 for id, 2 for username
			$this->found = false;
		}

		/* the method to do it all */
		public function doGetData() {
			if(!$this->dbcon) die('err001'); // the connection could not be made
			$isPresent = $this->getData();
			if($isPresent) $isPresent = $this->loadDbData();
			if($isPresent) $this->found = true;
		}
		/* the method which gets the data as it was requested, and cheks it */
		private function getData() {
			/* first we check to see if we have a "get by id" page, or a "get by username" one */
			$id_patt = '/[1-9][0-9]{0,17}/'; // should start with a 1, and the first 10000 are reserved numbers
			$uss_patt = '/[a-zA-Z][a-zA-Z0-9\\-_]{2,19}/';
			/* we escape the data to be sure */
			$this->data = mysqli_real_escape_string($this->dbcon, $this->data);
			if(preg_match($id_patt, $this->data)) {
				/* we have an id, so yeah... */
				$this->which = 1;
			} else if(preg_match($uss_patt, $this->data)) {
				/* we have an username */
				$this->which = 2;
			} else return $this->err("err1"); // not a valid id or username

			return true;
		}

		private function loadDbData() {
			/* now that we're sure we have either an id or username, we can continue loading the db data */
			$query = "";
			if($this->which == 1) {
				/* and we load the data by id */
				$id = $this->data;
				$query = "SELECT * FROM accounts WHERE aid=$id LIMIT 1"; // TODO: refine the search...

			} else if ($this->which == 2) {
				/* load the data by username */
				$username = $this->data;
				$query = "SELECT * FROM accounts WHERE LOWER(username)='$username' LIMIT 1"; // TODO: refine the search here too
			} else return $this->err('err4');
			/* and execute the query */
			if($query == "") return $this->err('err4'); // some error...
			$result = mysqli_query($this->dbcon, $query);
			if($result && !mysqli_num_rows($result)) return $this->err("err2"); // username or id not found
			$dbdata = mysqli_fetch_assoc($result);
			/* what remains to be done, is putting the data we got into our public var */
			if(!$dbdata) return $this->err('err4');
			$this->profile['username'] = $dbdata['username'];
			$this->profile['id'] = $dbdata['aid'];

			return true;
		}

		private function err($msg) {
			$this->errmsg = $msg;
			return false;
		}

		public function setDbCon($con) {
			$this->dbcon = $con;
		}
		public function setData($data) {
			$this->data = $data;
		}
	}

	include_once 'dbcon.php';

	$profile = new GetProfileData();
	$profile->setDbCon($dbcon);
?>