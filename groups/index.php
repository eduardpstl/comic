<?php
	include_once '../modules/session.php';
	include_once '../modules/groups.php';

	$session->doCheckSession();

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
		$groups->loadData(trim($id), '', $session->isLoggedIn, ($session->isLoggedIn) ? $session->public_data['id'] : 0);
		include_once 'a-group-page.php';
	} else if(isset($_SERVER['REQUEST_URI'])) {
		$request = explode('/',$_SERVER['REQUEST_URI']);  //die(var_dump($request));
		if(isset($request[3]) && trim($request[3]) == 'create') {
			if(!$session->isLoggedIn) $str = "meh"; // TODO : redirect, ca nu e logat
			else if(isset($_POST['name']) && isset($_POST['shortname']) && isset($_POST['description']) && isset($_POST['tags']) && isset($_POST['category']) && isset($_POST['privacy'])) {
				/* we have everything set, so we're actually looking at adding it into the db */
				$error = "";
				$groups->addGroup(trim($_POST['name']), trim($_POST['shortname']), trim($_POST['description']), trim($_POST['tags']), trim($_POST['category']), trim($_POST['privacy']), $session->public_data['id']);
			} else include_once '../includes/add-group.php'; // this means we're adding something to the db
		} else if(isset($request[3]) && strlen($request[3]) < 1 && !isset($request[4]))
			include_once '../includes/main-groups.php'; // we include the main page
		else if (isset($request[3]) && trim($request[3]) != "" && !isset($request[4])) { // we include a club page one
			$groups->loadData('', trim($request[3]), $session->isLoggedIn, ($session->isLoggedIn) ? $session->public_data['id'] : 0);
			include_once '../includes/group-page.php';
		} else if(isset($request[4]) && trim($request[4]) == 'edit' && isset($request[3]) && trim($request[3]) != '') {
			$groups->loadData('', trim($request[3]), $session->isLoggedIn, ($session->isLoggedIn) ? $session->public_data['id'] : 0);
			include_once '../includes/edit-group.php';
		} else header('Location: /comic/groups');
	} else {
		/* we see the main groups page */
		// nothing?
	}
?>